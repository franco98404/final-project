Feature: Register in a course

    As a student I want to register in a course so that I can access its content.
    
    Scenario: Verify as a student I can register in a course
        Given the student is logged into the platform
            And with the code "CKH9LE" to register in a course
        When the student click on the button Enroll in course
        Then the student is enrolled in the course