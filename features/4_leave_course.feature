Feature: Leave a course

    As a student I want to leave a course

    Scenario: Verify that a student can leave a course
        Given the student is logged into the platform
            And is enrolled in a course with ID "549404"
        When the student clicks in Drop this Course button
        Then the student is no longer enrolled in the course