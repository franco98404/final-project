Feature: Send a task

    As a student I want to send a task to a teacher.

    Scenario: Verify that a student can send a task
        Given the student is logged into the platform
            And is enrolled in a course with ID "549404"
            And is on the tasks page
        When the student clicks on "Task3"
            And the student should see the task details and click in Start Assignment button
            And complete the task
            And click in Submit Assignment button
        Then the student should see a success message