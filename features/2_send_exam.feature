Feature: Send an exam

    As a student I want to send a task to a teacher.

    Scenario: Verify that a student can send an exam
        Given the student is logged into the platform
            And is enrolled in a course with ID "549404"
            And is on the exams page
        When the student clicks on "Exam 3"
            And click in Take the Quiz button
            And click in Submit Quiz button
        Then the student should see the quiz score