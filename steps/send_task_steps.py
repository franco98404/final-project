from pages.tasks_page import TaskPage
from behave import *

use_step_matcher('re')

@step('is on the tasks page')
def go_tasks_page(context):
    tasks_page = TaskPage(context.webdriver)
    tasks_page.navigate_to_tasks_page()

@when('the student clicks on "(?P<task_name>.*?)"')
def click_task(context, task_name):
    tasks_page = TaskPage(context.webdriver)
    tasks_page.click_task(task_name)

@step('the student should see the task details and click in Start Assignment button')
def click_start_task(context):
    tasks_page = TaskPage(context.webdriver)
    tasks_page.click_start_task()

@step('complete the task')
def write_task(context):
    tasks_page = TaskPage(context.webdriver)
    tasks_page.write_task()

@step('click in Submit Assignment button')
def submit_task(context):
    tasks_page = TaskPage(context.webdriver)
    tasks_page.submit_task()

@then('the student should see a success message')
def check_task_success(context):
    tasks_page = TaskPage(context.webdriver)
    tasks_page.check_task_success()
