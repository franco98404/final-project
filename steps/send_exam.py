from pages.exam_page import ExamPage
from behave import *

use_step_matcher('re')

@step('is on the exams page')
def go_tasks_page(context):
    exam_page = ExamPage(context.webdriver)
    exam_page.navigate_to_exams_page()

@step('click in Take the Quiz button')
def click_start_exam(context):
    exam_page = ExamPage(context.webdriver)
    exam_page.click_start_exam()

@step('click in Submit Quiz button')
def submit_exam(context):
    exam_page = ExamPage(context.webdriver)
    exam_page.submit_exam()

@then('the student should see the quiz score')
def check_exam_success(context):
    exam_page = ExamPage(context.webdriver)
    exam_page.check_exam_success()