from pages.register_page import RegisterPage
from behave import *

use_step_matcher('re')

@step('with the code "(?P<code>.*?)" to register in a course')
def click_create(context, code):
    register_page = RegisterPage(context.webdriver)
    register_page.navigate_to_register_page(context.webapp_base_url, code)

@when('the student click on the button Enroll in course')
def click_enroll(context):
    register_page = RegisterPage(context.webdriver)
    register_page.enroll_in_course()

@then('the student is enrolled in the course')
def check_enroll(context):
    register_page = RegisterPage(context.webdriver)
    register_page.check_enroll_success()
