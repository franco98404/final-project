from pages.course_page import CoursePage
from behave import *

use_step_matcher('re')

@step('is enrolled in a course with ID "(?P<id>.*?)"')
def go_course_page(context, id):
    course_page = CoursePage(context.webdriver)
    course_page.navigate_to_course_page(context.webapp_base_url, id)

@when('the student clicks in Drop this Course button')
def click_drop_course(context):
    course_page = CoursePage(context.webdriver)
    course_page.click_leave_course()
    course_page.confirm_leave_course()

@then('the student is no longer enrolled in the course')
def check_drop_course(context):
    course_page = CoursePage(context.webdriver)
    course_page.check_leave_course_success()