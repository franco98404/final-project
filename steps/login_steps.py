from behave import *

from pages.login_page import LoginPage


@given('the student is logged into the platform')
def login_to_trello(context):
    login_page = LoginPage(context.webdriver)
    login_page.navigate_to_login_page(context.webapp_login_url)
    login_page.set_email(context.webapp_email)
    login_page.set_password(context.webapp_password)
    login_page.click_login()
    login_page.check_login_success()
