from pages.base_page import BasePage

class TaskPage(BasePage):

    def navigate_to_tasks_page(self):
        tasks_page = self._webdriver.find_by_xpath('//a[text()[contains(.,"Assignments")]]')
        tasks_page.click()

    def click_task(self, task_name):
        task_button = self._webdriver.find_by_xpath(f'//a[@class="ig-title"][text()[contains(.,"{task_name}")]]')
        task_button.click()

    def click_start_task(self):
        start_task_button = self._webdriver.find_by_xpath('//button[contains(@class,"submit_assignment_link")]')
        start_task_button.click()

    def write_task(self):
        frame = self._webdriver.find_by_xpath('//iframe[@id="submission_body_ifr"]')
        self._webdriver.switch_frame(frame)
        text_area = self._webdriver.find_by_id('tinymce')
        text_area.send_keys('This is a test')
        self._webdriver.switch_default()

    def submit_task(self):
        submit_button = self._webdriver.find_by_xpath('//button[@class="btn btn-primary"]')
        submit_button.click()

    def check_task_success(self):
        success_message = self._webdriver.find_by_xpath('//li[@class="ic-flash-success"]')
        assert success_message.is_displayed()

        

    



