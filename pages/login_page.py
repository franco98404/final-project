from pages.base_page import BasePage


class LoginPage(BasePage):

    def navigate_to_login_page(self, base_url):
        self._webdriver.navigate_to(base_url + '/login/canvas')

    def set_email(self, email):
        email_txt = self._webdriver.find_by_id('pseudonym_session_unique_id')
        email_txt.send_keys(email)

    def set_password(self, password):
        password_txt = self._webdriver.find_by_id('pseudonym_session_password')
        password_txt.send_keys(password)

    def click_login(self):
        login_btn = self._webdriver.find_by_xpath('//div[@class="ic-Form-control ic-Form-control--login"]/button')
        login_btn.click()

    def check_login_success(self):
        account_button = self._webdriver.find_by_id('global_nav_profile_link')
        account_button.click()
        logout_button = self._webdriver.find_by_xpath('//form[@action="/logout"]/button')
        logout_button.is_displayed()
