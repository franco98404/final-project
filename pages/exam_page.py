from pages.base_page import BasePage

class ExamPage(BasePage):

    def navigate_to_exams_page(self):
        exam_page = self._webdriver.find_by_xpath('//a[text()[contains(.,"Quizzes")]]')
        exam_page.click()

    def click_start_exam(self):
        start_exam_button = self._webdriver.find_by_id('take_quiz_link')
        start_exam_button.click()

    def submit_exam(self):
        submit_button = self._webdriver.find_by_id('submit_quiz_button')
        submit_button.click()

    def check_exam_success(self):
        success_message = self._webdriver.find_by_xpath('//div[text()[contains(.,"Quiz Score")]]')
        assert success_message.is_displayed()

    