from pages.base_page import BasePage

class CoursePage(BasePage):

    def navigate_to_course_page(self, base_url, code):
        self._webdriver.navigate_to(base_url + "/courses/" + code)

    def click_leave_course(self):
        leave_course_button = self._webdriver.find_by_xpath('//div[@id="course_show_secondary"]/a[contains(@class,"self_unenrollment_link")]')
        leave_course_button.click()

    def confirm_leave_course(self):
        confirm_button = self._webdriver.find_by_xpath('//div[@class="ui-dialog-buttonset"]/button[contains(@class,"btn btn-primary action")]')
        confirm_button.click()

    def check_leave_course_success(self):
        success_message = self._webdriver.find_by_xpath('//div[@id="course_show_secondary"]/a[contains(@class,"self_enrollment_link")]')
        success_message.is_displayed()
