from pages.base_page import BasePage

class RegisterPage(BasePage):

    def navigate_to_register_page(self, base_url, code):
        self._webdriver.navigate_to(base_url + "/enroll/" + code)

    def enroll_in_course(self):
        enroll_button = self._webdriver.find_by_xpath('//button[@class="btn btn-primary"]')
        enroll_button.click()

    def check_enroll_success(self):
        success_message = self._webdriver.find_by_xpath('//div[@id="enroll_form"]/p')
        text = success_message.text

        if text.startswith("You have successfully enrolled in") == False:
            assert False, "Enroll failed"
