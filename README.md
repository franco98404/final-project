Requirements:

1. Create a private repository in Bitbucket and add Jimmy.Maldonado@jalasoft.com as contributor (Make sure to push your commits every day)

2. Use Python as a programming language

3. Automate your tests cases using Gherkin

4. Each test case implemented must be independent of the others

5. Apply the design patterns that you may consider useful

6. Integrate the test automation framework with a logging tool.

7. Your tests should be able to run on the following browsers:

    o Local: Chrome and Firefox

    o Remote: Selenium Grid and any other cloud provider (Lambda test, Sauce labs, Browser Stack)

8. Your test automation framework should have unit tests for the functionality that you might implement.

9. The pre/post conditions need to be done through the API of the application

10. Your tests cases have to be executed using Jenkins and should meet the following points:

    o Use docker containers for the Jenkins server and the selenium hub

    o Execute the unit tests

    o Publish a report with the test results (in case of a failed step the report should show a screenshot)

    o The browser, remote provider (selenium grid or another cloud provider) and a filter to select which test has to be executed should be parameters in your Jenkins pipeline

    o Any sensitive data should be stored as a Jenkins credential

11. You need to prepare slides for your presentation, take into account the following aspects:

    o A brief explanation of the application/module that you are covering

    o The title of the test cases automated

    o The technology stack

    o Explain one of the test cases automated and the structure of your automation framework

    o Explain any design pattern applied that you used in your automation framework

    o Mention any challenges that you faced during the implementation of this project

    o Mention the best practices that you applied for this project