import allure
from allure_commons.types import AttachmentType
from helpers.selenium.webdriver_manager import WebDriverManager
from logger import Logger


def before_all(context):
    logs = Logger('Starting')
    logs.info("--------------- Initializing ---------------", is_title=True)
    logs.close()

    context.browser = context.config.userdata['browser']
    context.timeout_in_seconds = context.config.userdata['timeout_in_seconds']
    context.remote_url = context.config.userdata['remote_url']
    context.remote_provider = context.config.userdata['remote_provider']
    context.execution_type = context.config.userdata['execution_type']

    context.webapp_login_url = context.config.userdata['webapp_login_url']
    context.webapp_base_url = context.config.userdata['webapp_base_url']
    context.webapp_email = context.config.userdata['webapp_email']
    context.webapp_password = context.config.userdata['webapp_password']

    context.restapi_base_url = context.config.userdata['restapi_base_url']
    context.restapi_token = context.config.userdata['restapi_token']

    context.webdriver = WebDriverManager(context.browser, context.timeout_in_seconds,
                                         context.execution_type, context.remote_provider, context.remote_url)


def before_scenario(context, scenario):
    context.params = {'token': context.restapi_token}
    logs = Logger('Started Scenario')
    logs.info(scenario.name)
    logs.close()


def after_scenario(context, scenario):
    logs = Logger('Finished Scenario')
    logs.info(scenario.name)
    logs.info("", is_title=True)
    logs.close()


def after_all(context):
    logs = Logger('Finishing')
    logs.info("--------------- All the files were executed ---------------", is_title=True)
    logs.info("", is_title=True)
    context.webdriver.close()


def after_step(context, step):
    logs = Logger('Step')
    if step.status == 'failed':
        logs.error(step.name)
        allure.attach(context.webdriver.get_screenshot_as_png(), 'screenshot', AttachmentType.PNG)
    else:
        logs.info(step.name)
    logs.close()